class UsersController < ApplicationController
  def index
    zendesk_api = ZendeskApi.new()
    @users = zendesk_api.get_users()
    render :index
  end

  def show
    id = params[:id]
    zendesk_api = ZendeskApi.new()
    @user = zendesk_api.get_user(id)
  end
end
