class TicketsController < ApplicationController
  def convert_page_query_to_int(page)
    page_int = 1
    if !page
      page_int = 1
    elsif page.to_i < 1
      page_int = 1
    else
      page_int = page.to_i
    end
    return page_int
  end

  def index
    page = convert_page_query_to_int(params[:page])
    zendesk_api = ZendeskApi.new()
    @sort_by = params[:sort_by]
    status_filter = params[:status_filter]
    @sort_order = params[:sort_order]

    @tickets = get_tickets_from_api(zendesk_api, page, @sort_by, status_filter)

    render :index
  end

  def get_tickets_from_api(zendesk_api, page, sort_by, status_filter)
    tickets = Hash.new

    if !status_filter
      tickets_obj = zendesk_api.get_tickets(page, sort_by)

      if !tickets_obj["error_message"]
        tickets = tickets_obj["tickets"]
      else
        @error_message = tickets_obj["error_message"]
      end
    else
      tickets_obj = zendesk_api.filter_tickets(status_filter)
      if !tickets_obj["error_message"]
        tickets = tickets_obj["results"]
      else
        @error_message = tickets_obj["error_message"]
      end
    end

    if !tickets_obj["error_message"]
      @tickets_count = tickets_obj["count"]
      @current_page_start_index = tickets_obj["current_page_start_index"]
      @current_page_end_index = tickets_obj["current_page_end_index"]
      @record_per_page = 25
      @num_of_pages = (@tickets_count / @record_per_page.to_f).ceil
      if page.to_i > @num_of_pages
        @error_message = "This page doesn't exit."
      end
    else
      @error_message = tickets_obj["error_message"]
    end

    return tickets
  end

  def show
    id = params[:id]
    zendesk_api = ZendeskApi.new()
    ticket_detail = zendesk_api.get_ticket_detail(id)
    if !ticket_detail["error_message"]
      @ticket = ticket_detail["ticket"]
      user_id = @ticket["requester_id"]
      @user = zendesk_api.get_user(user_id)
    else
      @error_message = ticket_detail["error_message"]
    end
    render :show
  end
end
