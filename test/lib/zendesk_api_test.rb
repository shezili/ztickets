require "minitest/autorun"
require "minitest/reporters"
require "rails/all"
require_relative "../../lib/zendesk_api"
Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

class ZendeskApiTest < ActiveSupport::TestCase
  def setup
    fakeAuth = {:username => "fake/token", :password => "fake"}
    @zendeskApi = ZendeskApi.new(fakeAuth, "http://faketest.come")
  end

  def test_get_404_error_message
    assert_equal "The page you're looking for could not be found.", @zendeskApi.get_error_message(404)
  end

  def test_get_401_error_message
    assert_equal "You are unauthorized to access the requested resource, Please check your email address and an API token.", @zendeskApi.get_error_message(401)
  end

  def test_get_500_error_message
    assert_equal "ERROR 500. Try again later", @zendeskApi.get_error_message(500)
  end

  def test_get_current_page_start_index
    assert_equal 1, @zendeskApi.get_current_page_start_index(0, 25)
  end

  def test_get_current_page_start_index
    assert_equal 26, @zendeskApi.get_current_page_start_index(2, 25)
  end

  def test_get_current_page_end_index
    assert_equal 50, @zendeskApi.get_current_page_end_index(2, 25, 106)
  end

  def test_get_current_page_end_index
    assert_equal 106, @zendeskApi.get_current_page_end_index(5, 25, 106)
  end
end
