require "minitest/autorun"
require "minitest/reporters"

require "rails/all"
require_relative "../../app/controllers/tickets_controller.rb"
require_relative "../../lib/zendesk_api"
Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

class TicketsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @ticketsController = TicketsController.new()
    fakeAuth = {:username => "fake/token", :password => "fake"}
    @zendeskApi = ZendeskApi.new(fakeAuth, "http://faketest.come")
  end

  def test_convert_page_query_to_int_nil_to_1
    assert_equal 1, @ticketsController.convert_page_query_to_int(nil)
  end

  def test_convert_page_query_to_int_0_to_1
    assert_equal 1, @ticketsController.convert_page_query_to_int(0)
  end

  def test_convert_page_query_to_int_sting_to_int
    assert_equal 3, @ticketsController.convert_page_query_to_int("3")
  end

  test "should get index" do
    get "/tickets"
    assert_response :success
  end

  test "should get Ajax call for pagination" do
    get "/tickets", params: {page: 3}, xhr: true
    assert_response :success
  end

  test "should go to detail-show page" do
    get "/tickets/:id", params: {id: 52}
    assert_response :success
  end
end
