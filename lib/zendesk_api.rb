class ZendeskApi
  def initialize(auth = nil, base_url = nil)
    if !auth
      @auth = {:username => Rails.application.secrets.USERNAME_EMAIL + "/token", :password => Rails.application.secrets.API_TOKEN}
    end
    if !base_url
      @base_url = APP_CONFIG["z_base_uri"]
    end
    @page_size = 25
  end

  def filter_tickets(status_filter)
    filter_url = "#{@base_url}search.json?query=type%3Aticket+status%3A#{status_filter}"
    begin
      tickets = Hash.new
      res = HTTParty.get(filter_url, :basic_auth => @auth)
      error_message = get_error_message(res.code)
      if (error_message)
        tickets["error_message"] = error_message
      else
        tickets = res.parsed_response
      end
    rescue HTTParty::Error => e
      tickets["error_message"] = "Faild to connect to zendesk api " + e.message
    rescue StandardError => e
      tickets["error_message"] = "Error " + e.message
    end
    return tickets
  end

  def get_tickets(page, sort_by)
    url = "#{@base_url}tickets.json?page=#{page}&per_page=#{@page_size}&sort_by=#{sort_by}&status=open"
    begin
      tickets = Hash.new
      res = HTTParty.get(url, :basic_auth => @auth)
      # note to self, i am using rufo for code formater https://github.com/ruby-formatter/rufo
      # validate response status code, check if it is 200, if not return errr
      error_message = get_error_message(res.code)
      if (error_message)
        ticket_detail["error_message"] = error_message
      else
        tickets = res.parsed_response
        tickets_count = tickets["count"]
        tickets["current_page_start_index"] = get_current_page_start_index(page.to_i, @page_size)
        tickets["current_page_end_index"] = get_current_page_end_index(page.to_i, @page_size, tickets_count)
      end
    rescue HTTParty::Error => e
      tickets["error_message"] = "Faild to connect to zendesk api " + e.message
    rescue StandardError => e
      tickets["error_message"] = "Error " + e.message
    end
    return tickets
  end

  def get_user(id)
    user_url = "#{@base_url}users/#{id}.json"
    begin
      user = Hash.new
      res = HTTParty.get(user_url, :basic_auth => @auth)
      error_message = get_error_message(res.code)
      if (error_message)
        user["error_message"] = error_message
      else
        user = res.parsed_response
      end
    rescue HTTParty::Error => e
      user["error_message"] = "Faild to connect to zendesk api " + e.message
    rescue StandardError => e
      user["error_message"] = "Error " + e.message
    end
    return user["user"]
  end

  def get_users()
    users_url = "#{@base_url}users.json"
    begin
      users = Hash.new
      users = HTTParty.get(users_url, :basic_auth => @auth)
      error_message = get_error_message(res.code)
      if (error_message)
        users["error_message"] = error_message
      else
        users = res.parsed_response
      end
    rescue HTTParty::Error => e
      users["error_message"] = "Faild to connect to zendesk api " + e.message
    rescue StandardError => e
      users["error_message"] = "Error " + e.message
    end
    return users["users"]
  end

  def get_ticket_detail(id)
    detail_url = "#{@base_url}tickets/#{id}.json"
    begin
      ticket_detail = Hash.new
      res = HTTParty.get(detail_url, :basic_auth => @auth)
      # note to self, i am using rufo for code formater https://github.com/ruby-formatter/rufo
      # validate response status code, check if it is 200, if not return errr
      error_message = get_error_message(res.code)
      if (error_message)
        ticket_detail["error_message"] = error_message
      else
        ticket_detail = res.parsed_response
      end
    rescue HTTParty::Error => e
      ticket_detail["error_message"] = "Faild to connect to zendesk api " + e.message
    rescue StandardError => e
      ticket_detail["error_message"] = "Error " + e.message
    end
    return ticket_detail
  end

  def get_error_message(status_code)
    error_message = nil
    case status_code
    when 401
      error_message = "You are unauthorized to access the requested resource, Please check your email address and an API token."
    when 404
      error_message = "The page you're looking for could not be found."
    else
      if status_code != 200
        error_message = "ERROR #{status_code}. Try again later"
      end
    end
    return error_message
  end

  def get_current_page_start_index(page, page_size)
    if page == 0
      page = 1
    end
    return (page - 1) * page_size + 1
  end

  def get_current_page_end_index(page, page_size, tickets_count)
    start_index = get_current_page_start_index(page, page_size)
    end_index = start_index + page_size - 1
    if tickets_count < end_index
      return tickets_count
    else
      return end_index
    end
  end
end
