Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # root to:'pages#home'
  get "/", to: "tickets#index"
  get "/tickets", to: "tickets#index"
  get "/tickets/index.js", to: "tickets#index"
  get "/tickets/:id", to: "tickets#show"
  get "/error", to: "pages#error"
  get "/users", to: "users#index"
  get "/users/:id", to: "users#show"
end
