# Ticket Viewer
[Design Document](https://docs.google.com/document/d/1HCdIY_l6okN17mFFnjXQm98c7F2ATnni4lMDekDdn2w/edit?usp=sharing)

This is a tickets viewer designed to show zendesk tickets information of a organization.

User can view list of tickets on the home page, which is status-sorted and paged for every 25 records. Details page will show once users click on any of tickets in the list. And in details page, it will show details of selected ticket.

I’ve also made the website responsive, which means its mobile device friendly.

## Demo
[Tickets Viewer Demo](https://gentle-tor-30402.herokuapp.com/)

## Local Dev Environment Setup
* ruby 2.4.3p205
* Rails 5.1.6

## Dependencies
###  Required
 use 
 ```bundle install``` 
 to install all required dependencies
### Optional
I am using rufo to format my code, so get rufo gem to make the code formating consistent.

## Configure zendesk api endpoint
Zendesk api endpoint is configured at config/config.yml
default value is 
``` 
z_base_uri: https://zoomflag.zendesk.com/api/v2/
```

## Rails Env Config 
 Configure local private environment variables, and all required environment variables is defined in a `config/environment.yml` file and these will then be loaded into ENV each time your application starts. Just remeber that restart server after config modification.  

Following entries are required for this app:
``` 
API_TOKEN: YOUR_ZENDESK_API_TOKEN
USERNAME_EMAIL: YOUR_ZENDESK_USER_NAME
```

Be sure that you don't commit this file into your repository. It should only be used for setting configuration for development use. you can either manually add environment.yml into .gitignore, or run the command below.

```bash
echo 'config/environment.yml' >> .gitignore
```

## Run server
Run the server and go to http://localhost:3000/ 
```bash
rails server
```

## Test
* Minitest

    Just need to run
```bash 
rails test 
```

## Resources
* tickets list page: 

    ``` /tickets ```
 
    or 

    ``` /tickets?page={pageNumber}```

* ticket details page: /tickets/{id}

## Deployment 
I am using heroku for my prod environment, and it's just simple as run following command to deploy:  
```bash
git push heroku master
```

Note: you will need to setup  git remote for heroku first before you can run this command. see link below for details.  
https://devcenter.heroku.com/articles/git

## Gotchas
* Do Not Use ​Javascript in the browser (running on Node is fine). You won’t be able to successfully complete a GET request as we prevent cross-domain requests.

* Remember that even though the response comes back as JSON format it is still just a string and needs to be parsed to be of any use to you.

* Use basic authentication.